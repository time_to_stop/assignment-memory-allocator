#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define INITIAL_HEAP_SIZE REGION_MIN_SIZE

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

static struct region alloc_region(void const * addr, size_t query) {
    bool extends = true;
    size_t actual_query = region_actual_size(query);
    void* region_addr = map_pages(addr, actual_query, MAP_FIXED_NOREPLACE);

    if (region_addr == MAP_FAILED) {
        extends = false;
        region_addr = map_pages(addr, actual_query, 0);
    }

    if (region_addr == MAP_FAILED) return REGION_INVALID;

    block_size size = {actual_query };
    block_init(region_addr, size, NULL);

    return (struct region) {
        .addr = region_addr,
        .size = actual_query,
        .extends = extends
    };
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

static size_t block_actual_size(size_t query) {
    return size_max( query, BLOCK_MIN_CAPACITY );
}

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (!block_splittable(block, query)) return false;

    block_size new_size = {block->capacity.bytes - query};
    void* next_addr = block->contents + query;

    block_init(next_addr , new_size, block->next);
    block->next = next_addr;
    block->capacity.bytes = query;

    return true;
}

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if (block->next == NULL || !mergeable(block, block->next)) return false;

    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;

    return true;
}

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    for (; block != NULL; block = block->next){
        if (block_is_big_enough(sz, block) && block->is_free) {
            return (struct block_search_result) {
                    .block = block,
                    .type = BSR_FOUND_GOOD_BLOCK
            };
        }

        if (block->next == NULL) {
            return (struct block_search_result) {
                    .block = block,
                    .type = BSR_REACHED_END_NOT_FOUND
            };
        }
    }
    return (struct block_search_result) {
            .block = NULL,
            .type = BSR_CORRUPTED
    };
}

static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result result = find_good_or_last(block, query);

    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query);
    }

    return result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    struct region extend = alloc_region(block_after(last), query + offsetof(struct block_header, contents));

    if (region_is_invalid(&extend)) return NULL;

    extend.extends = extend.addr == block_after(last);
    last->next = extend.addr;

    while (last != NULL && last->next != NULL) {
        if (mergeable(last, last->next)) {
            while(last->next != NULL) {
                if (!try_merge_with_next(last)) {
                    break;
                }
            }
        }
        last = last->next;
    }

    struct block_search_result search_of_grown = try_memalloc_existing(query, HEAP_START);

    if (search_of_grown.type == BSR_FOUND_GOOD_BLOCK) return search_of_grown.block;

    return NULL;
}


static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    size_t actual_size = block_actual_size(query);

    struct block_search_result result_in_existing = try_memalloc_existing(query, heap_start);

    switch(result_in_existing.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return result_in_existing.block;
        case BSR_REACHED_END_NOT_FOUND:
            return grow_heap(heap_start, actual_size);
        case BSR_CORRUPTED:
        default:
            return NULL;
    }
}

void* _malloc( size_t query ) {
    struct block_header* const allocated_block = memalloc( query, (struct block_header*) HEAP_START );

    if (!allocated_block) return NULL;

    allocated_block->is_free = false;
    return allocated_block->contents;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return;

  struct block_header* header = block_get_header( mem );
  header->is_free = true;

  while(header->next != NULL) {
      if (!try_merge_with_next(header)) {
          break;
      }
  }
}
