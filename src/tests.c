#include "tests.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define TEST_FAILED EXIT_FAILURE
#define TEST_SUCCESS EXIT_SUCCESS

typedef int (*test_t)(struct block_header*);

static int test_mem(void* ptr, int len)
{
    const unsigned int max_value = 255;
    const unsigned int size = len / sizeof(uint8_t);
    uint8_t* const array = (uint8_t*)ptr;

    for(unsigned int i = 0; i != size; i++)
    {
        array[i] = (i + 1) % max_value;
    }

    for(unsigned int i = 0; i != size; i++)
    {
        if (array[i] != (i + 1) % max_value)
        {
            return TEST_FAILED;
        }
    }

    return TEST_SUCCESS;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static struct block_header* alloc(int size)
{
    void* data = _malloc(size);

    if (data == NULL)
    {
        printf("Failed to allocate memory with len %d", size);
        return NULL;
    }

    if (test_mem(data, size) == TEST_FAILED)
    {
        printf("Failed to test allocated memory");
        return NULL;
    }

    return block_get_header(data);
}

static int test1(struct block_header* heap)
{
    const int len = 200 * sizeof(uint8_t);

    debug_heap(stdout, heap);

    struct block_header* t = alloc(len);

    debug_heap(stdout, heap);

    if (!t)
    {
        return TEST_FAILED;
    }

    if (t->is_free)
    {
        printf("Allocated block is free");
        return TEST_FAILED;
    }

    if (test_mem(t->contents, len) == TEST_FAILED)
    {
        _free(t->contents);
        return TEST_FAILED;
    }

    _free(t->contents);

    printf("Memory after free\n");
    debug_heap(stdout, heap);

    return TEST_SUCCESS;
}

#define TEST_SIZE 3

static int test2(struct block_header* heap)
{
    const int len[TEST_SIZE] =  { 200 * sizeof(uint8_t), 400 * sizeof(uint8_t), 9000 * sizeof(uint8_t) };
    struct block_header* t[TEST_SIZE] = { 0 };

    debug_heap(stdout, heap);

    for (int i = 0; i != TEST_SIZE; i++)
    {
        t[i] = alloc(len[i]);
    }

    debug_heap(stdout, heap);

    _free(t[1]->contents);

    debug_heap(stdout, heap);

    for (int i = 0; i != TEST_SIZE; i++)
    {
        if (!t[i]->is_free && test_mem(t[i]->contents, len[i]) != TEST_SUCCESS)
        {
            _free(t[0]->contents);
            _free(t[2]->contents);

            return TEST_FAILED;
        }
    }

    _free(t[0]->contents);
    _free(t[2]->contents);

    debug_heap(stdout, heap);

    return TEST_SUCCESS;
}


static int test3(struct block_header* heap)
{
    const int len[TEST_SIZE] =  { 9000 * sizeof(uint8_t), 33000 * sizeof(uint8_t), 1234 * sizeof(uint8_t) };
    struct block_header* t[TEST_SIZE] = { 0 };

    debug_heap(stdout, heap);

    for (int i = 0; i != TEST_SIZE; i++)
    {
        t[i] = alloc(len[i]);
    }

    debug_heap(stdout, heap);

    _free(t[1]->contents);
    _free(t[2]->contents);

    debug_heap(stdout, heap);

    for (int i = 0; i != TEST_SIZE; i++)
    {
        if (!t[i]->is_free && test_mem(t[i]->contents, len[i]) != TEST_SUCCESS)
        {
            _free(t[0]->contents);

            return TEST_FAILED;
        }
    }

    _free(t[0]->contents);

    debug_heap(stdout, heap);

    return TEST_SUCCESS;
}


static int test4(struct block_header* heap)
{
    debug_heap(stdout, heap);

    const int size = 100000;
    struct block_header* t = alloc(size);

    if (t == NULL || t->is_free == true)
    {
        printf("Failed to allocate size %d\n", size);
        return TEST_FAILED;
    }

    _free(t->contents);

    debug_heap(stdout, heap);
    return TEST_SUCCESS;
}

int test()
{
    const size_t heap_size = 65536;
    struct block_header* test_heap = heap_init(heap_size);
    test_t tests[] = { &test1, &test2, &test3, &test4 };

    if (!test_heap)
    {
        printf("Failed to allocate test heap\n");
        return TEST_FAILED;
    }

    for (int i = 0; i != sizeof(tests)/sizeof(test_t); i++)
    {
        printf("Test %d\n", i + 1);

        const size_t code = tests[i](test_heap);

        if (code == TEST_FAILED)
        {
            printf("Test %d: FAILED\n", i + 1);
            return TEST_FAILED;
        }

        printf("Test %d: PASSED\n", i + 1);
    }


    return TEST_SUCCESS;
}